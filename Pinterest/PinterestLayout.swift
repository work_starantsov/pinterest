//
//  PinterestLayout.swift
//  Pinterest
//
//  Created by Nazar Starantsov on 10/08/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

protocol PinterestDelegate: class {
    func collectionView(_ collectionView: UICollectionView, numberOfColumns: Int, heightForItemAtIndexPath indexPath: IndexPath) -> CGFloat
}

class PinterestLayout: UICollectionViewFlowLayout {
    weak var delegate: PinterestDelegate!
    
    fileprivate var cacheAttrubutes = [UICollectionViewLayoutAttributes]()
    
    fileprivate let numberOfColumns = 2
    fileprivate var contentHeight: CGFloat = 0
    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else { return 0.0 }
        
        // We decrease width by insets so we can use insets on collection view
        // and width will be calculated knowing about those insets
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { return }
        let columnWidth: CGFloat = self.contentWidth / CGFloat(numberOfColumns)
        
//        let xOffset = [0, columnWidth]
//        let yOffset = [0, 0]
        var xOffset = [CGFloat]()
        for column in 0 ..< self.numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var yOffset: [CGFloat] = [CGFloat](repeating: 0, count: self.numberOfColumns)
        
        var columnToPlace = 0
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            // Even items positioned at second column, odd at first
            // let columnToPlace1 = item % 2 == 0 ? 1 : 0
            
            let indexPath = IndexPath(item: item, section: 0)
            
            // Using method inside collectionView that has access to images heights which we need to get value of
            let photoHeight: CGFloat = self.delegate.collectionView(collectionView, numberOfColumns: self.numberOfColumns, heightForItemAtIndexPath: indexPath)
            
            let frame = CGRect(x: xOffset[columnToPlace], y: yOffset[columnToPlace], width: columnWidth, height: photoHeight)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = frame
            self.cacheAttrubutes.append(attributes)
            
            // y offses for each column is stored here and increased by photo all the time
            yOffset[columnToPlace] += photoHeight
            columnToPlace = columnToPlace < (numberOfColumns - 1) ? (columnToPlace + 1) : 0
        }
    }
    
    /// This function layouts each cell (sets up size and position for each one)
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        // Variable cacheAttributes already has filled array with attributes for each cell
        // because UICollectionViewFlowLayout (which we conform to) is running prepare method for us
        // prepare method is overriten by us and it appends all needed attributes inside cacheAttributes
        // by the type that function is called
        return self.cacheAttrubutes
    }
}
