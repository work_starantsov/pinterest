//
//  PinterestCell.swift
//  Pinterest
//
//  Created by Nazar Starantsov on 11/08/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class PinterestCell: UICollectionViewCell {
    var image: UIImage! {
        didSet {
            imageView.image = image
        }
    }
    
    fileprivate var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 12
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    fileprivate lazy var horizontalStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [label, dots])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.axis = .horizontal
        return stack
    }()
    
    fileprivate lazy var verticalStackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [imageView, horizontalStackView])
        stack.axis = .vertical
        return stack
    }()
    
    fileprivate let dots: UIButton = {
        let dots = UIButton()
        dots.translatesAutoresizingMaskIntoConstraints = false
        dots.setImage(UIImage(named: "dots")?.withRenderingMode(.alwaysTemplate), for: .normal)
        dots.imageView?.contentMode = .scaleAspectFit
        dots.imageView?.tintColor = .darkGray
        return dots
    }()
    fileprivate let label: UILabel = {
        let label = UILabel()
        label.text = "Check it out."
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        contentView.addSubview(verticalStackView)
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        verticalStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        verticalStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        verticalStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        horizontalStackView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        dots.widthAnchor.constraint(equalToConstant: 24).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 6, left: 4, bottom: 6, right: 4))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
