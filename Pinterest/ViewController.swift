//
//  ViewController.swift
//  Pinterest
//
//  Created by Nazar Starantsov on 10/08/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    fileprivate let images = [ "a1", "a2", "a4", "a5", "a2", "a6", "a1", "a7", "a3" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = PinterestLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collection = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.register(PinterestCell.self, forCellWithReuseIdentifier: "pinterestCell")
        collection.dataSource = self
        collection.showsVerticalScrollIndicator = false
        collection.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 2000, right: 0)
        // Since ViewController conforms to PinterestDelegate protocol(implemented method 'heightForItemAtIndexPath')
        // we set our custom property delegate inside PinterestLayout class to self
        // This gives us ability to call method declared in ViewController by PinterestLayout class like delegate.collectionView(....
        if let layout = collection.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
        view.addSubview(collection)
    }
}


extension ViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pinterestCell", for: indexPath) as! PinterestCell
        cell.image = UIImage(named: self.images[indexPath.item])
        return cell
    }
}

extension ViewController: PinterestDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfColumns: Int, heightForItemAtIndexPath indexPath: IndexPath) -> CGFloat {
        let image = UIImage(named: self.images[indexPath.item])!
        let imageHeight = image.size.height
        let imageWidth = image.size.width
        let imageRatio = (collectionView.frame.width / CGFloat(numberOfColumns)) / imageWidth
        return imageHeight * imageRatio
    }
}

